/* parsiranje unetog teksta */
function parsirajText(){
	/* hvatanje teksta */
	var txt = document.getElementById("txar").value;

	/* pretraga za linkovima */
	txt = txt.replace(/(https?:\/\/[^\s]+)/g,function(account){
		return '<a href="' + account + '">' + account + '</a>';
	});
	/* pretraga za recima sa @ */
	txt = txt.replace(/@(\w){1,15}/g,function(account){
		account = account.replace("@","");
  		return '<a href="http://twitter.com/' + account + '">' +  '<img src="./img/twitter.png" alt="Twiti"/>' +account + '</a>';
	});
	/* pretraga sa recima koje pocinju sa # */
	txt = txt.replace(/#(\w){1,15}/g,function(account){
		account = account.replace("#","");
  		return '<a href="http://twitter.com/hashtag/' + account + '">#' + account + '</a>';
	});

	/* prikazivanje parsiranog teksta */
	document.getElementById("result").innerHTML = txt;
}